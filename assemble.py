#!/usr/bin/env python
import string
import xml.etree.ElementTree as ET

ns = {"svg": "http://www.w3.org/2000/svg"}

ET.register_namespace("svg", ns["svg"])

template = ET.parse('template.svg')
root = template.getroot()
font = root.find("svg:font", ns)

for l in string.ascii_uppercase:
    charSvg = ET.parse('%s.svg' % l)
    path = charSvg.find("svg:path", ns)
    glyph = ET.SubElement(font, "svg:glyph",{})
    glyph.set("unicode", l)
    glyph.append(path)

print(ET.tostring(root))
